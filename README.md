# README #

### An experimental implementation of confidential DNS.###

Overview
-----------
This  project implements  an opportunistic encryption protocol extension for the DNS. The protocol uses a new opCode for encrypted queries, and a mechanism for public key distribution.
######Compatibility######
The added option has no effect if the server does not support the   encryption protocol, which will result in a normal DNS response.   When  encryption is supported, this extension ensures that communication   with known servers will always be encrypted.
######Transport over UDP/TCP######
There are no changes to the transport mode (UDP or TCP), except a slight incre is packet size when the server's public key is included.   
######Version: 0.2######
*  Changes from 0.1: encrypting the DNS header (minus flag bits) in addition to the other sections.
  -  It is possible to guess with a significant confidence the domain name in a query by observing the DNS header of the response alone, because depending on how the server is configured, RRSets with a few RRs (more than one, and small enough to fit in a single UDP response) can be used to characterize a domain by observing some information in the DNS header:
      -  number of Answer RRs
      -  number of Authority RRs
  -  Hence the need to encrypt the DNS header.


### Setup ###

####Configuration####
1.  Refer to the example schema in conf/resolvconf.xml for configuration.
2.  Prepare the app's directory: /etc/encrypted-dns
3.  Move the configuration files (conf/resolvconf.xml and conf/log4j.xml) to /etc/encrypted-dns.
4.  The server daemon must have access to /etc/encrypted-dns, where it stores keys.
####Dependencies ####
1. [dnsjava](http://www.xbill.org/dnsjava/)
2. [Bouncy Castle](http://www.cs.berkeley.edu/~jonah/bc/overview-summary.html)
3. [Apache Commons](http://commons.apache.org/proper/commons-codec/)
4. [log4j](http://logging.apache.org/log4j/2.x/)
####Database configuration ####
-None
####How to run tests####
-Note: server or client will create key files if not present.
#####Project:#####
-run crypt.dns.test.Main
#####Integration#####
  1. update configurations in /etc/encrypted-dns/conf/resolvconf.xml 
  2. run crypt.privacy.Server
  3. send DNS queries to the DNS port configured in resolvconf.xml
  4. to retrieve server's public key, set type to KEY.
#####Deployment instructions#####
1. build pom (goal: package)
2. Copy /conf to /etc/encrypted-dns
3. Edit configurations in /etc/encrypted-dns/conf/resolvconf.xml
4. deploy jar

### Contribution guidelines ###

* Writing tests
>Coming soon
* Code review
>Not projected
* Other guidelines
>Bug reports are welcome!

### Who do I talk to? ###

* Repo owner or admin
>No info released
* Other community or team contact
>No info released