package crypt.dns.test;

import dns.crypt.privacy.Resolvconf;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.util.Random;
import java.util.UUID;

import javax.crypto.NoSuchPaddingException;

import org.apache.log4j.Logger;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.xbill.DNS.Lookup;
import org.xbill.DNS.Record;

import dns.crypt.privacy.EncryptingResolver;
import dns.crypt.privacy.LoggerConf;
import dns.crypt.privacy.NSDaemon;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import java.util.Map;

import org.xbill.DNS.Resolver;
import org.xbill.DNS.SimpleResolver;

public class Main {
	static Logger log = Logger.getLogger(Main.class);
	
	public static void main(String[] arg) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IOException, NoSuchProviderException, 
                InvalidAlgorithmParameterException, InvalidKeySpecException, InvalidCipherTextException, ClassNotFoundException{
		boolean tcp = false;
		LoggerConf.init(Resolvconf.LOG_CONFIG_PATH);
                Map<String, Integer> dnsPorts = Resolvconf.getDNSPorts();
		String[] names = {"google.rw", "smartrwanda.org", "mail.logix.rw", "google.com", "yahoo.com", "skype.com", "microsoft.ca"};
		//NSDaemon ns = new NSDaemon(InetAddress.getLocalHost(), null, Arrays.asList("8.8.4.4"), tcp);
		//ns.start();
		EncryptingResolver resolver = new EncryptingResolver(InetAddress.getLocalHost().getHostName(), Resolvconf.isStubModeEncryptionEnabled());
		resolver.enableCachedSecrets(false);
		Random rand = new Random();
		int port = rand.nextInt(2000) + 1111;
                if(tcp){
                    if(dnsPorts.containsKey("tcp")){
                        resolver.setPort(dnsPorts.get("tcp"));
                        resolver.setTCP(true);
                    }else{
                        log.fatal("No TCP port configured for DNS!");   
                        System.exit(1);
                    }
                }else if(dnsPorts.containsKey("udp")){
                    resolver.setPort(dnsPorts.get("udp"));
                    resolver.setTCP(false);
                }else{
                    log.fatal("No UDP port configured for DNS!");   
                    System.exit(1);
                }
		long start = System.currentTimeMillis();
		int repeat = 100;
		for(int i=0; i<repeat; i++){
			String name = names[i%names.length];
			Lookup l = new Lookup(name);
			l.setResolver(resolver);
			l.run();
			if(l.getResult() == Lookup.SUCCESSFUL){
				for(Record r: l.getAnswers()){
					log.debug(r.toString());
					break;
				}
			}else{
				log.debug("Error: " + l.getErrorString());
			}
		}
		long end = System.currentTimeMillis();
		/*try {
			ns.shutdown();
			log.debug("Time run: " + (end - start)/1000.0 + " seconds.");
			ns.join();
		} catch (InterruptedException e) {
			log.error(e.getMessage());
			System.exit(1);
		}*/
		log.debug("Time run: " + (end - start)/1000.0 + " seconds.");
	}
}
