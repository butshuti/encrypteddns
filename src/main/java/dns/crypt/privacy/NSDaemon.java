package dns.crypt.privacy;
import java.io.*;
import java.net.*;
import java.security.NoSuchAlgorithmException;

import java.security.spec.InvalidKeySpecException;

import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
 
public class NSDaemon extends Thread {
 
    protected DatagramSocket udpSocket = null;
    protected ServerSocket tcpSocket = null;
    private AsymmetricKeyParameter privKey;
    private static final short DEFAULT_UDPSIZE = 512;
    private static final short DEFAULT_UDP_THREAD_POOL_SIZE = 20;
    private boolean running = true;
    private boolean tcp = false;
    private int server_port = -1;
    private final static String DEFAULT_NAME = "computer.";
    private ThreadPoolExecutor poolExecutor;
    private int threadPoolSize;
    private byte[] pubKeyBytes;
    private String defaultName;
    private Logger log = Logger.getLogger(this.getClass());
    private Map<String, Boolean> upstreams;
    private Map<String, Integer> dnsPorts;
    
    public NSDaemon(InetAddress address, String name) throws IOException, NoSuchAlgorithmException,
                                                                                                  InvalidKeySpecException {
    	super(name == null || name.isEmpty()? DEFAULT_NAME : name);
        this.upstreams = Resolvconf.getUpstreams();
        if(upstreams == null || upstreams.isEmpty()){
            log.error("No upstreams to query. Exiting with error.");
        }
        dnsPorts = Resolvconf.getDNSPorts();
        if(dnsPorts.containsKey("udp_and_tcp")){
        	if(!(this.initTCP(address, name) && this.initUDP(address, name))){
                System.exit(1);
            }
        }else if(dnsPorts.containsKey("tcp")){
            if(!this.initTCP(address, name)){
                System.exit(1);
            }
        }else{
            if(!this.initUDP(address, name)){
                System.exit(1);
            }
        }
    }
    
    private boolean initUDP(InetAddress address, String name) throws IOException, NoSuchAlgorithmException,
                                                                  InvalidKeySpecException {
        if(!(dnsPorts.containsKey("udp") || dnsPorts.containsKey("udp_and_tcp"))){
            log.fatal("DNS port not configured for UDP.");
            return false;
        }else{
            server_port = dnsPorts.get("udp");
        }
    	this.defaultName = name == null || name.isEmpty()? DEFAULT_NAME : name;
        threadPoolSize = DEFAULT_UDP_THREAD_POOL_SIZE ;
        poolExecutor = new ThreadPoolExecutor(this.threadPoolSize, this.threadPoolSize, 1, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());
        udpSocket = new DatagramSocket(server_port, address);
        udpSocket.setReuseAddress(true);
        privKey = PKIUtils.loadPrivateKeyKeyFromFile(Resolvconf.getPrivateKeyPath());
        pubKeyBytes = PKIUtils.readRawKeyFile(Resolvconf.getPublicKeyPath());
        this.setDaemon(true);
        return true;
    }
    
    private boolean initTCP(InetAddress address, String name) throws IOException, NoSuchAlgorithmException,
                                                                  InvalidKeySpecException {
        if(!(dnsPorts.containsKey("tcp") || dnsPorts.containsKey("udp_and_tcp"))){
            log.fatal("DNS port not configured for TCP.");
            return false;
        }else{
            server_port = dnsPorts.get("tcp");
        }
        this.defaultName = name == null || name.isEmpty()? DEFAULT_NAME : name;
        threadPoolSize = DEFAULT_UDP_THREAD_POOL_SIZE ;
        poolExecutor = new ThreadPoolExecutor(this.threadPoolSize, this.threadPoolSize, 1, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());
        tcpSocket = new ServerSocket(server_port, this.threadPoolSize, address);
        tcpSocket.setReuseAddress(true);
        this.tcp = true;
        privKey = PKIUtils.loadPrivateKeyKeyFromFile(Resolvconf.getPrivateKeyPath());
        pubKeyBytes = PKIUtils.readRawKeyFile(Resolvconf.getPublicKeyPath());
        this.setDaemon(true);
        return true;
    }
    
    public void setPoolSize(int size){
    	this.threadPoolSize = size;
    }
    
    @Override
    public void run() {
        while (isRunning()) {
        	byte[] in = new byte[DEFAULT_UDPSIZE];
			DatagramPacket packet = new DatagramPacket(in, in.length);
			packet.setLength(in.length);
			try {
				if(!tcp){
					udpSocket.receive(packet);
					this.poolExecutor.execute(new UDPConnection(defaultName, this.udpSocket, packet, privKey, pubKeyBytes, upstreams));
				}else{
					Socket socket = tcpSocket.accept();
					this.poolExecutor.execute(new TCPConnection(defaultName, socket, privKey, pubKeyBytes, upstreams));
				}
			} catch (IOException e) {
				log.error(e.getMessage());
			}
        }
        if(udpSocket !=null)udpSocket.close();
        if(tcpSocket !=null)
			try {
				tcpSocket.close();
			} catch (IOException e) {
				log.error(e.getMessage());
			}
    }
    
    private boolean isRunning() {
		return running && !this.isInterrupted();
    }

    public void shutdown() throws InterruptedException{
    	running = false;
    	if(udpSocket !=null)udpSocket.close();
        if(tcpSocket !=null)
			try {
				tcpSocket.close();
			} catch (IOException e) {
				log.error(e.getMessage());
			}
    	log.debug("Shutting down....!");
    	Thread.currentThread().interrupt();
    }
}