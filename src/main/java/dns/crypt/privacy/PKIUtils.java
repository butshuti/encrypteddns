package dns.crypt.privacy;

import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.UnknownHostException;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;

import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;

import java.security.spec.X509EncodedKeySpec;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Base64OutputStream;
import org.apache.log4j.Logger;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.crypto.util.PrivateKeyFactory;
import org.bouncycastle.crypto.util.PublicKeyFactory;

public class PKIUtils {

	private static HashMap<String, String> localTrust;
	private static boolean initialized = false;
	public final static String KEYSTORE_BASE_PATH = System.getProperty("user.home", "")+"/.encrypted-dns";
	public final static String KEY_STORE_DB_PATH = KEYSTORE_BASE_PATH + File.separator + "resolvers.conf";
	private static Logger log = Logger.getLogger(PKIUtils.class);
	
	static{
	    Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
            if(localTrust == null){
                    localTrust = new HashMap<String, String>();
                    try {
                            Scanner sc = new Scanner(new File(KEY_STORE_DB_PATH));
                            String server[];
                            int count = 0;
                            while(sc.hasNextLine()){
                                    server = sc.nextLine().trim().split(" ");
                                    if(server.length == 0 || server[0].isEmpty()){
                                            continue;
                                    }
                                    if(server.length == 2){
                                            localTrust.put(server[0],  server[1]);
                                            count++;
                                    }else{
                                            log.error("Malformed keystore: + " + KEY_STORE_DB_PATH + "; line " + count + " [" + server[0] + "]");
                                            System.exit(1);
                                    }
                            }
                            sc.close();
                    } catch (FileNotFoundException e) {
                            log.error("No key store found for trusted resolvers!");
                    }
            }
	}
	
	public static AsymmetricKeyParameter fetchHostKey(String hostname) throws UnknownHostException,
                                                                              NoSuchAlgorithmException,
                                                                              InvalidKeySpecException{
		String keyFilePath = localTrust.get(hostname+".");
                log.debug("====================" + hostname + " => " + keyFilePath);
		if(keyFilePath != null){
			try {
				return loadPublicKeyKeyFromFile(keyFilePath);
			} catch (IOException e) {
				String msg = String.format("Problem when parsing public key for %s from file %s.", hostname, keyFilePath);
				//throw new UnknownHostException(msg);
                                throw new RuntimeException(e);
			}
		}else{
			return null;
		}
	}

	public static void initialize() {
		if(!initialized){
			initialized = true;
			Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
                        CryptoUtils.init();
		}		
	}

	public static boolean createKeyPair(String pubKeyPath, String privKeyPath){
		try {
			KeyPairGenerator gntr = KeyPairGenerator.getInstance("RSA", "BC");
			SecureRandom random = new SecureRandom();
			gntr.initialize(1024, random);
			KeyPair kp = gntr.generateKeyPair();
			PublicKey pubKey = kp.getPublic();
			PrivateKey privKey = kp.getPrivate();
			BufferedWriter out = new BufferedWriter(new FileWriter(pubKeyPath));
                        out.write(new String(Base64.encodeBase64(pubKey.getEncoded())));
			out.close();
			out = new BufferedWriter(new FileWriter(privKeyPath));
			out.write(new String(Base64.encodeBase64(privKey.getEncoded())));
			out.close();
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	public static String saveRawKeyInfo(byte[] keyBytes, String key, long ttl){
                if(!check_keystore_basedir()){
                    return null;
                }
		try {
			String keyFile = KEYSTORE_BASE_PATH + File.separator + key + String.valueOf(ttl) + ".key";
			File keyStoreDir = new File(KEYSTORE_BASE_PATH);
			if(!keyStoreDir.isDirectory()){
				keyStoreDir.mkdir();
			}
                        DataOutputStream dos = new DataOutputStream(new FileOutputStream(new File(keyFile)));
                        dos.write(Base64.encodeBase64(keyBytes));
                        dos.close();
			return (new File(keyFile).exists() && updateHostKeys(key, keyFile)) ? keyFile : null;
		} catch (Exception e) {
			log.error(e.getMessage());
			return null;
		}
	}
	
        private static boolean check_keystore_basedir(){
            if(!(new File(KEYSTORE_BASE_PATH).isDirectory())){
                return new File(KEYSTORE_BASE_PATH).mkdirs();
            }
            return true;
        }
    
	private static boolean updateHostKeys(String hostName, String keyFile) throws IOException {
		localTrust.put(hostName, keyFile);
                if(!check_keystore_basedir()){
                    return false;
                }
		BufferedWriter out = new BufferedWriter(new FileWriter(KEY_STORE_DB_PATH, false));
		for(String key : localTrust.keySet()){
			out.write(key + " " + localTrust.get(key) + "\n");
		}
		out.close();
		return (new File(KEY_STORE_DB_PATH).exists());
	}

	public static AsymmetricKeyParameter loadPublicKeyKeyFromFile(String path) throws IOException,
                                                                                      NoSuchAlgorithmException,
                                                                                      InvalidKeySpecException {
        File keyFile = new File(path);
        DataInputStream dis = new DataInputStream(new FileInputStream(keyFile));
        byte[] keyBytes = new byte[(int)keyFile.length()];
        dis.readFully(keyBytes);
        dis.close();
        X509EncodedKeySpec spec = new X509EncodedKeySpec(Base64.decodeBase64(keyBytes));
        KeyFactory keyFactory;
        try {
            keyFactory = KeyFactory.getInstance("RSA", "BC");
        } catch (NoSuchProviderException e) {
            throw new RuntimeException(e);
        }
        RSAPublicKey pubKey = (RSAPublicKey) keyFactory.generatePublic(spec);
        AsymmetricKeyParameter key = new RSAKeyParameters(false, pubKey.getModulus(), pubKey.getPublicExponent());
        return key;
	}
	
	public static AsymmetricKeyParameter loadPrivateKeyKeyFromFile(String path) throws IOException,
                                                                                       NoSuchAlgorithmException,
                                                                                       InvalidKeySpecException {
        File keyFile = new File(path);
        DataInputStream dis = new DataInputStream(new FileInputStream(keyFile));
        byte[] keyBytes = new byte[(int)keyFile.length()];
        dis.readFully(keyBytes);
        dis.close();
        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(Base64.decodeBase64(keyBytes));
        KeyFactory keyFactory;
        try {
            keyFactory = KeyFactory.getInstance("RSA", "BC");
        } catch (NoSuchProviderException e) {
            throw new RuntimeException(e);
        }
        RSAPrivateKey privKey = (RSAPrivateKey) keyFactory.generatePrivate(spec);
        AsymmetricKeyParameter key = new RSAKeyParameters(true, privKey.getModulus(), privKey.getPrivateExponent());
        return key;
	}

        public static byte[] readRawKeyFile(String path) throws IOException{
                File keyFile = new File(path);
                DataInputStream dis = new DataInputStream(new FileInputStream(keyFile));
                byte[] out = new byte[(int)keyFile.length()];
                dis.readFully(out);
                return Base64.decodeBase64(out);
        }
        
	private static String readFile(String path) throws FileNotFoundException{
		Scanner sc = new Scanner(new File(path));
		sc.useDelimiter("\\Z");
		String ret = sc.next();
		sc.close();
		return ret;
	}
        
    private byte[] noopEncode(byte[] in){
        return in;
    }
    
    private byte[] noopDecode(byte[] in){
        return in;
    }
}
