package dns.crypt.privacy;

import java.security.Key;
import java.security.NoSuchAlgorithmException;

import javax.crypto.spec.SecretKeySpec;

import org.apache.log4j.Logger;

public class KeySpec {

	private int algorithmID;
	private long ttl;
	private String owner;
	private byte[] keyBytes;
	private boolean isSecret;
	private Logger log = Logger.getLogger(this.getClass());
	
	enum KeyType{ASYMMETRIC, SYMMETRIC};
	
	public KeySpec(byte[] keyBytes, int alg, long ttl, String owner, boolean isSecret){
		this.algorithmID = alg;
		this.ttl = ttl;
		this.isSecret = isSecret;
		this.keyBytes = keyBytes;
		this.owner = owner;
	}
	
	public Key getKey(){
		if(isSecret){
			try {
				return new SecretKeySpec(keyBytes, 0, keyBytes.length, Enc.AlgorithmSpec.fromBits(algorithmID).getName());
			} catch (NoSuchAlgorithmException e) {
				log.error(e.getLocalizedMessage());
				return null;
			}
		}
		return null;
	}
	public byte[] getKeyBytes(){
		return keyBytes;
	}
	public int getAlgorithm(){return algorithmID;}
	public boolean isSecret(){return isSecret;}
	public long getTTL(){return ttl;}
	public String getOwner(){return owner;}
}
