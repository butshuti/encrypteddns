package dns.crypt.privacy;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.AlgorithmParameters;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Arrays;
import java.util.Scanner;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

public class CryptoUtils {
	
	public static final String KEY_DIR = "/home/butshuti/dump/crypto_files";
	public static final String DUMB_KEY_FILE = KEY_DIR + "/keyfile.file";
	public static final String CLIENT_PUBLIC_KEY = KEY_DIR + "/client_key.public";
	public static final String CLIENT_PRIVATE_KEY = KEY_DIR + "/client_key.private";
	public static final String SERVER_PUBLIC_KEY = KEY_DIR + "/server_key.public";
	public static final String SERVER_PRIVATE_KEY = KEY_DIR + "/server_key.private";
	public static final String CERT_INFOS_FILE = KEY_DIR + "/cert_infos.txt";
	
	public static final String SERVER_CERT_PRIVATE_KEY_FILE = KEY_DIR + "/certprivate.pem";
	public static final String SERVER_CERT_FILE = KEY_DIR + "/cert.der";
	
	public static final String SIGNATURE_ALGOTIRHM = "SHA1WithRSA";
	public static final String CRYPTO_DES = "DES";
	public static final String CRYPTO_RSA = "RSA";
	private static final String CERT_GENERATION_TEMPLATE = "openssl req -new -x509 -keyform PEM -key %s -outform DER -out %s < %s";
	
	/*static{
		System.err.println(
				  "\t+==============================================================================+"
				+ "\n\t| Initializing the Crypto directory.   A new server certificate and keys will  |"
				+ "\n\t| get created each time from cert_infos.txt. This file should exist!           |"
				+ "\n\t| NOTE THAT IT IS ASSUMED THAT /bin/sh IS AVAILABLE IN THE RUNTIME ENVIRONMENT.|"
				+ "\n\t| Client Secret key and key pair will be created as well.......................+");
		if(!new File(KEY_DIR).isDirectory()){
			new File(KEY_DIR).mkdir();
		}
		if(!new File(CLIENT_PUBLIC_KEY).exists()){
			try {
				createKeyPairFiles(CLIENT_PUBLIC_KEY, CLIENT_PRIVATE_KEY);
			} catch (Exception e) {
				System.err.println(String.format("Unable to create CLIENT keys: %s\n", e.getMessage()));
				System.exit(1);
			}
		}
		if(!new File(SERVER_PUBLIC_KEY).exists()){
			try {
				createKeyPairFiles(SERVER_PUBLIC_KEY, SERVER_PRIVATE_KEY);
			} catch (Exception e) {
				System.err.println(String.format("Unable to create SERVER keys: %s\n", e.getMessage()));
				System.exit(1);
			}
		}
		if(!new File(CryptoUtils.DUMB_KEY_FILE).exists()){
			try {
				CryptoUtils.saveKey(CryptoUtils.generateSecretKey(CRYPTO_DES), CryptoUtils.DUMB_KEY_FILE);
			} catch (IOException e) {
				System.err.println(String.format("Unable to create client's secret key: %s\n", e.getMessage()));
				System.exit(1);
			} catch (NoSuchAlgorithmException e) {
				System.err.println(String.format("Unable to create client's secret key: %s\n", e.getMessage()));
				System.exit(1);
			}
		}
		System.err.println(
				  "\t| Initialization complete!                                                     |"
				+ "\n\t+==============================================================================+\n\n\n");
	}*/
	private static byte[] process(byte[] msgBytes, Key key, boolean encrypt) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IOException, InvalidAlgorithmParameterException{
		byte[] cryptBytes;
		Cipher cipher;
		try {
                        cipher = Cipher.getInstance(key.getAlgorithm());
			cipher.init(encrypt? Cipher.ENCRYPT_MODE : Cipher.DECRYPT_MODE, key);
			cryptBytes = cipher.doFinal(msgBytes);
		} catch (IllegalBlockSizeException e1) {
			e1.printStackTrace();
			return null;
		} catch (BadPaddingException e1) {
			e1.printStackTrace();
			return null;
		}
		return cryptBytes;
	}

	protected static byte[] encrypt(byte[] msgBytes, Key key) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IOException{
		return process(msgBytes, key, true);
	}
	
	protected static byte[] decrypt(byte[] msgBytes, Key key) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IOException{
		return process(msgBytes, key, false);
	}
	
	protected static byte[] sign(byte[] data, PrivateKey privKey) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException{
		Signature signer = Signature.getInstance(SIGNATURE_ALGOTIRHM);
		signer.initSign(privKey);
		signer.update(data);
		return signer.sign();
	}
	
	protected static boolean verifySignature(byte[] data, byte[] signature, PublicKey pubKey) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException{
		Signature signer = Signature.getInstance(SIGNATURE_ALGOTIRHM);
		signer.initVerify(pubKey);
		signer.update(data);
		return signer.verify(signature);
	}
	
	protected static void saveKey(Key key, String fileName) throws IOException{
		ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(fileName));
		os.writeObject(key);
		os.close();
	}
	
	protected static Key parseFileKey(String fileName) throws ClassNotFoundException, IOException{
		ObjectInputStream is = new ObjectInputStream(new FileInputStream(fileName));
		Key key = (Key)is.readObject();
		is.close();
		return key;
	}
	
	protected static void createKeyPairFiles(String pathToPublic, String pathToPrivate) throws NoSuchAlgorithmException, IOException {
		KeyPairGenerator kpg = KeyPairGenerator.getInstance(CRYPTO_RSA);
		KeyPair kp = kpg.generateKeyPair();
		saveKey(kp.getPrivate(), pathToPrivate);
		saveKey(kp.getPublic(), pathToPublic);		
	}
	
	protected static String readFile(String path) throws FileNotFoundException{
		Scanner sc = new Scanner(new File(path));
		sc.useDelimiter("\\Z");
		String ret = sc.next();
		sc.close();
		return ret;
	}
	
	protected static SecretKey generateSecretKey(String keyForm) throws NoSuchAlgorithmException{
		KeyGenerator keygenerator = KeyGenerator.getInstance(keyForm);
		return keygenerator.generateKey();
	}
	
	protected static void init(){}
	
	protected static Certificate loadServerCert() throws CertificateException, IOException, InterruptedException{
		String privateKeyFileTemplate = String.format("openssl genrsa -out %s 1024", SERVER_CERT_PRIVATE_KEY_FILE);
		String certKeyFileTemplate = String.format(CERT_GENERATION_TEMPLATE, SERVER_CERT_PRIVATE_KEY_FILE, SERVER_CERT_FILE, CERT_INFOS_FILE);
		Process p = Runtime.getRuntime().exec(privateKeyFileTemplate);
		p.waitFor();
		p = Runtime.getRuntime().exec(new String[]{"/bin/sh", "-c", certKeyFileTemplate});
		p.waitFor();
		InputStream is = new FileInputStream(SERVER_CERT_FILE);
		CertificateFactory factory = CertificateFactory.getInstance("X.509");
		Certificate cert = factory.generateCertificate(is);
		is.close();
		return cert;
	}
}
