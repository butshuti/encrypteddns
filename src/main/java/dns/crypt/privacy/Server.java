package dns.crypt.privacy;

import crypt.dns.test.Main;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.NoSuchAlgorithmException;

import java.security.spec.InvalidKeySpecException;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.commons.configuration.XMLConfiguration;

import org.apache.log4j.Logger;

public class Server {

        static Logger log = Logger.getLogger(Main.class);
        
        
	public static void main(String[] args) throws NoSuchAlgorithmException, UnknownHostException, IOException,
                                                  InvalidKeySpecException {
            
		LoggerConf.init(Resolvconf.LOG_CONFIG_PATH);
                String appConfigPath = Resolvconf.APP_CONFIG_PATH;
                /*if(args.length != 1){
                    log.error("Error: config file expected as first argument. Exiting with error.");
                    System.exit(1);
                }else if(!(new File(args[0]).exists())){
                    log.error(String.format("Error: config file %s not found. Exiting with error.", args[0]));
                    System.exit(1);
                }else{
                    appConfigPath = args[0];
                }*/
                XMLConfiguration config = null;
                List<String> upstreams = new LinkedList<String>();
                try{
                    config = new XMLConfiguration(appConfigPath);
                }catch(ConfigurationException e){
                    log.error(e.getMessage());
                    System.exit(1);
                }
                List fields = config.configurationsAt("forwarders.resolver");
                for( Iterator it = fields.iterator(); it.hasNext(); ){
                    HierarchicalConfiguration sub = (HierarchicalConfiguration)it.next();
                    if(sub.getString("[@ip]") != null){
                        upstreams.add(sub.getString("[@ip]"));
                    }
                }
                if(upstreams == null || upstreams.isEmpty()){
                    log.error("No forwarders found. Please check the configurations.");
                    System.exit(1);
                }
                System.setProperty("java.net.preferIPv4Stack" , "true");
                InetAddress serverAddress = null;
                serverAddress = InetAddress.getByName(Resolvconf.getServerAddress());
                NSDaemon ns = new NSDaemon(serverAddress, null);
		RecentClients.enableCache(false);
		ns.start();
		try {
			ns.join();
		} catch (InterruptedException e) {
			System.exit(0);
		}
	}
}
