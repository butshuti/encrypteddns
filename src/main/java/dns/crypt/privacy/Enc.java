package dns.crypt.privacy;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;

import org.apache.log4j.Logger;
import org.bouncycastle.crypto.AsymmetricBlockCipher;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.encodings.PKCS1Encoding;
import org.bouncycastle.crypto.engines.AESEngine;
import org.bouncycastle.crypto.engines.RSAEngine;
import org.bouncycastle.crypto.paddings.PKCS7Padding;
import org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.params.KeyParameter;

public class Enc {

	protected static int RECORDS_START = 0;
	private static int IV_LEN = 16;
	private static SecureRandom secRandom;
	protected static IvParameterSpec ivSpec;
	public final static String DEFAULT_PADD_MODE = "NoPadding";
	protected static Cipher cipher;
	protected static Logger log = Logger.getLogger(Enc.class);
	
	static{
		
	}
	
	public enum AlgorithmSpec{
		AES(1),
		DES(2),
		RSA(3),
		DSA(4),
		ECDSA(5);
		
		public static final int MASK = 0x0C;
		private int id;
		private AlgorithmSpec(int idd){
			id = idd;
		}
		
		public static AlgorithmSpec fromBits(int spec) throws NoSuchAlgorithmException{
			for(AlgorithmSpec alg : AlgorithmSpec.values()){
				if(alg.getSpec() == (spec & MASK) ){
					return alg;
				}
			}
			throw new NoSuchAlgorithmException(String.format("Unknown algorithm: <%s>",  Integer.toHexString(spec)));
		}
		
		public int getSpec(){
			int offs = MASK, ret = id;
			while(offs > 0 && (offs & 1) == 0){
				offs >>= 1;
				ret <<= 1;
			}
			return ret;
		}
		
		public static int getSpecID(int spec){
			int offs = MASK, ret = spec;
			while(offs > 0 && (offs & 1) == 0){
				offs >>= 1;
				ret >>= 1;
			}
			return ret;
		}
		
		public String getName(){
			return name();
		}
		
		@Override
		public String toString(){
			return getName() + "<" + Integer.toBinaryString(getSpec()) + ">";
		}
	}
	public enum ModeSpec{
		CTR(1),
		CBC(2),
		ECB(3),
		OAEP(4);
		
		public static final int MASK = 0x03;
		private int id;
		
		ModeSpec(int idd){
			id = idd;
		}
		public int getSpec(){
			int offs = MASK, ret = id;
			while(offs > 0 && (offs & 1) == 0){
				offs >>= 1;
				ret <<= 1;
			}
			return ret;
		}
		
		public static ModeSpec fromBits(int spec) throws NoSuchAlgorithmException{
			for(ModeSpec mode : ModeSpec.values()){
				if(mode.getSpec() == (spec & MASK) ){
					return mode;
				}
			}
			throw new NoSuchAlgorithmException("Unknown mode: " + Integer.toHexString(spec));
		}
		
		public String getName(){
			return name();
		}
		@Override
		public String toString(){
			return getName() + "<" + Integer.toBinaryString(getSpec()) + ">";
		}
	}
	
	public enum EncMode{
		DEFAULT(AlgorithmSpec.AES.getSpec()| ModeSpec.CTR.getSpec()),
		RSA(AlgorithmSpec.RSA.getSpec() | ModeSpec.ECB.getSpec()),
		DSA(AlgorithmSpec.DSA.getSpec() | ModeSpec.OAEP.getSpec()),
		AES_CTR(AlgorithmSpec.AES.getSpec()| ModeSpec.CTR.getSpec()),
		AES_CBC(AlgorithmSpec.AES.getSpec()| ModeSpec.CBC.getSpec()),
		AES_ECB(AlgorithmSpec.AES.getSpec()| ModeSpec.ECB.getSpec()),
		DES_CTR(AlgorithmSpec.DES.getSpec()| ModeSpec.CTR.getSpec()),
		DES_CBC(AlgorithmSpec.DES.getSpec()| ModeSpec.CBC.getSpec()),
		DES_ECB(AlgorithmSpec.DES.getSpec()| ModeSpec.ECB.getSpec());
		
		private int spec;
		
		EncMode(int specInt){
			spec = specInt;
		}
		
		public int getID(){
			return spec;
		}
		public String getEncryptionAlg() throws NoSuchAlgorithmException{
			return AlgorithmSpec.fromBits(spec).name();
		}
		public String getName() throws NoSuchAlgorithmException{
			AlgorithmSpec alg = AlgorithmSpec.fromBits(spec);
			ModeSpec mode = ModeSpec.fromBits(spec);
			return "(" + Integer.toBinaryString(spec) + "): {" + alg.toString() + " with " + mode.toString() + "}";
		}
		
		public static String getName(int specInt) throws NoSuchAlgorithmException{
			AlgorithmSpec alg = AlgorithmSpec.fromBits(specInt);
			ModeSpec mode = ModeSpec.fromBits(specInt);
			return "(" + Integer.toBinaryString(specInt) + "): {" + alg.toString() + " with " + mode.toString() + "}";
		}
		
		public String getForm(String paddMode) throws NoSuchAlgorithmException{
			return getForm(spec, paddMode);
		}
		
		public static String getForm(int specInt, String paddMode) throws NoSuchAlgorithmException{
			AlgorithmSpec alg;
			ModeSpec mode;
			alg = AlgorithmSpec.fromBits(specInt);
			mode = ModeSpec.fromBits(specInt);
			StringBuilder sb = new StringBuilder(alg.name());
			sb.append("/");
			sb.append(mode.name());
			sb.append("/");
			sb.append(paddMode);
			return sb.toString();
		}
	}
	
	static{
		secRandom = new SecureRandom();
		byte[] iv = new byte[IV_LEN];
		secRandom.nextBytes(iv);
		ivSpec = new IvParameterSpec(iv);
	}

	protected static byte[] encrypt(byte[] msgBytes, Key key, int alg, boolean useIV) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IOException, InvalidAlgorithmParameterException{
                return CryptoUtils.encrypt(msgBytes, key);
	}
	
	protected static byte[] decrypt(byte[] msgBytes, Key key, int alg, boolean useIV) 
			throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IOException, InvalidAlgorithmParameterException{
		return CryptoUtils.decrypt(msgBytes, key);
	}
	
	protected static byte[] AsymBCProcess(byte[] inputBytes, AsymmetricKeyParameter key, boolean encrypt) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IOException, InvalidCipherTextException{
		AsymmetricBlockCipher bc = new PKCS1Encoding(new RSAEngine());
		bc.init(encrypt, key);
		int inputOffs=0, outputOffs=0, blockSize=bc.getInputBlockSize(), outBS=bc.getOutputBlockSize(), inputLen=inputBytes.length;
		int outputLen;
		if(encrypt){
			outputLen = (outBS/blockSize)*inputLen;
			outputLen += (outBS - (outputLen % outBS));
		}else{
			outputLen = (blockSize/outBS)*inputLen;
			outputLen += (outBS - (outputLen % outBS));
		}
		byte[] outputBytes = new byte[outputLen];
		while(inputOffs < inputLen){
			blockSize = Math.min(blockSize, inputLen - inputOffs);
			byte[] temp = bc.processBlock(inputBytes, inputOffs, blockSize);
			System.arraycopy(temp, 0, outputBytes, outputOffs, temp.length);
			inputOffs += blockSize;
			outputOffs += temp.length;
			if(temp.length < bc.getOutputBlockSize())break;
		}
		byte[] ret = new byte[outputOffs];
		System.arraycopy(outputBytes, 0, ret, 0, outputOffs);
		return ret;
	}
        
    protected static byte[] symBCProcess(byte[] inputBytes, KeyParameter key, boolean encrypt) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IOException, InvalidCipherTextException{
            PaddedBufferedBlockCipher bc = new PaddedBufferedBlockCipher(new AESEngine(), new PKCS7Padding());
            new PKCS1Encoding(new RSAEngine());
            bc.init(encrypt, key);
            int outputLen;
            byte[] ret = new byte[bc.getOutputSize(inputBytes.length)];
            outputLen = bc.processBytes(inputBytes, 0, inputBytes.length, ret, 0);
            bc.doFinal(ret, outputLen);
            return ret;
    }
	
	public static byte[] asymEncrypt(byte[] msgBytes, AsymmetricKeyParameter key) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IOException, InvalidCipherTextException{
            return AsymBCProcess(msgBytes, key, true);
	}
	
	public static byte[] asymDecrypt(byte[] msgBytes, AsymmetricKeyParameter key) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IOException, InvalidCipherTextException{
		return AsymBCProcess(msgBytes, key, false);
	}
	
	public static byte[] symEncrypt(byte[] msgBytes, Key key, int alg)  throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IOException, InvalidCipherTextException{
                byte[] aa = symBCProcess(msgBytes, new KeyParameter(key.getEncoded()), true);
                byte[] bb = symBCProcess(aa, new KeyParameter(key.getEncoded()), false);
                return symBCProcess(msgBytes, new KeyParameter(key.getEncoded()), true);
	}
	
	public static byte[] symDecrypt(byte[] msgBytes, Key key, int alg)  throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IOException, InvalidCipherTextException{
                return symBCProcess(msgBytes, new KeyParameter(key.getEncoded()), false);
	}
}
