package dns.crypt.privacy;

import java.io.File;

import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import java.util.Map;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.commons.configuration.XMLConfiguration;
import org.apache.log4j.Logger;

import org.bouncycastle.crypto.params.AsymmetricKeyParameter;

public class Resolvconf {
    public static final String APP_CONFIG_DIR = "/etc/encrypted-dns";
    private static final String DEFAULT_CONFIG_DIR = "/etc/encrypted-dns";
    public static final String LOG_CONFIG_PATH = DEFAULT_CONFIG_DIR + File.separator + "conf" + File.separator + "log4j.xml";
    public static final String APP_CONFIG_PATH = DEFAULT_CONFIG_DIR + File.separator + "conf" + File.separator + "resolvconf.xml";
    private static Logger log = Logger.getLogger(Resolvconf.class);
    private static Map<String, Integer> dnsPorts = new HashMap<String, Integer>();
    private static Map<String, String> policy = new HashMap<String, String>();
    private static Map<String, Boolean> upstreams = new HashMap<String, Boolean>();
    private static AsymmetricKeyParameter privateKey, publicKey;
    private static boolean enableStubModeEncryption = true;
    private static boolean enableClientKeyCache = false;
    private static String publicKeyPath, privateKeyPath;
    private static String serverAddress = "";
    
    static final int CONFIG_DEFAULT_PORT = 53;
    static final String CONFIG_KEY_NAME = "[@name]";
    static final String CONFIG_KEY_VALUE = "[@value]";
    static final String CONFIG_KEY_IP = "[@ip]";
    static final String CONFIG_KEY_PRIVACY = "[@privacy]";
    static final String CONFIG_KEY_PRIVATE = "[@private]";
    static final String CONFIG_KEY_PATH = "[@path]";
    static final String STUB_MODE_ENCRYPTION = "[@stub_mode_encryption]";
    
    
    static{
        synchronized(Resolvconf.class){
            PKIUtils.initialize();
            LoggerConf.init(LOG_CONFIG_PATH);
            XMLConfiguration config = null;
            try{
                config = new XMLConfiguration(APP_CONFIG_PATH);
            }catch(ConfigurationException e){
                log.error(e.getMessage());
                System.exit(1);
            }
            parseKeys(config);
            parseConnectionPrefs(config);
            parseUpstreams(config);
            parseResolverPolicy(config);
            log.error(dnsPorts);
            log.error(upstreams);
            //parseConfigEntity(config, "client_server_policy.policy");
        }
    }
    
    public static Map<String, Boolean> getUpstreams(){
        return upstreams;
    }
    
    public static Map<String, Integer> getDNSPorts(){
        return dnsPorts;
    }
    
    public static String getServerAddress(){
    	return serverAddress;
    }
    
    public static String getPublicKeyPath(){
        return publicKeyPath;
    }
    
    public static String getPrivateKeyPath(){
        return privateKeyPath;
    }
    
    public static boolean isStubModeEncryptionEnabled(){
        return enableStubModeEncryption;
    }
    
    private static boolean parseConnectionPrefs(XMLConfiguration config){
        List fields = config.configurationsAt("connection_prefs.pref");
        dnsPorts.clear();
        for( Iterator it = fields.iterator(); it.hasNext(); ){
            HierarchicalConfiguration sub = (HierarchicalConfiguration)it.next();
            String entityName = sub.getString(CONFIG_KEY_NAME);
            if(entityName.equals("dns_ports")){
                int port = sub.getInt(CONFIG_KEY_VALUE);
                String protocol = sub.getString("[@protocol]");
                if(protocol.equals("udp") || protocol.equals("tcp")){
                    dnsPorts.put(protocol, port);
                }else if(protocol.equals("udp_and_tcp")){
                    dnsPorts.put("udp", port);
                    dnsPorts.put("tcp", port);
                }
            }else if(entityName.equals("listen_address")){
            	serverAddress = sub.getString(CONFIG_KEY_IP);
            }
        }
        return (!dnsPorts.isEmpty()) && addressBelongsToLocalHost(serverAddress);
    }
    
    private static boolean parseUpstreams(XMLConfiguration config){
        List fields = config.configurationsAt("forwarders.resolver");
        upstreams.clear();
        for( Iterator it = fields.iterator(); it.hasNext(); ){
            HierarchicalConfiguration sub = (HierarchicalConfiguration)it.next();
            if(sub.getString(CONFIG_KEY_IP) != null){
                boolean requestPrivacy = sub.getBoolean(CONFIG_KEY_PRIVACY);
                upstreams.put(sub.getString(CONFIG_KEY_IP), requestPrivacy);
            }
        }
        return !upstreams.isEmpty();
    }
    
    private static boolean parseKeys(XMLConfiguration config){
        List fields = config.configurationsAt("keys.key");
        upstreams.clear();
        publicKeyPath = null;
        privateKeyPath = null;
        for( Iterator it = fields.iterator(); it.hasNext(); ){
            HierarchicalConfiguration sub = (HierarchicalConfiguration)it.next();
            if(sub.getString(CONFIG_KEY_PATH) != null){
                if(sub.getBoolean(CONFIG_KEY_PRIVATE)){
                    if(privateKeyPath != null){
                        log.error("Private key already initialized.");
                    }else{
                        privateKeyPath = sub.getString(CONFIG_KEY_PATH);
                    }
                }else{
                    if(publicKeyPath != null){
                        log.error("Public key already initialized.");
                    }else{
                        publicKeyPath = sub.getString(CONFIG_KEY_PATH);
                    }
                }
            }
        }
        if(publicKeyPath == null || privateKeyPath == null){
            log.fatal("Could not parse configurations for encryption keys.");
            System.exit(1);
        }else{
            if(!new File(publicKeyPath).exists()){
                    try {
                            PKIUtils.createKeyPair(publicKeyPath, privateKeyPath);
                    } catch (Exception e) {
                            log.fatal(String.format("Unable to create SERVER keys: %s\n", e.getMessage()));
                            System.exit(1);
                    }
            }
            try{
                return (PKIUtils.loadPrivateKeyKeyFromFile(privateKeyPath) != null &&
                    PKIUtils.loadPublicKeyKeyFromFile(publicKeyPath) != null);
            }catch (IOException e) {
                log.fatal(String.format("Unable to create RESOLVER keys: %s\n", e.getMessage()));
                System.exit(1);
                }catch(InvalidKeySpecException e){
                    log.fatal(String.format("Unable to create RESOLVER keys: %s\n", e.getMessage()));
                    System.exit(1);
                }catch(NoSuchAlgorithmException e){
                    log.fatal(String.format("Unable to create RESOLVER keys: %s\n", e.getMessage()));
                    System.exit(1);
                }
        }
        return false;
    }
    
    private static boolean parseResolverPolicy(XMLConfiguration config){
        List fields = config.configurationsAt("client_server_policy.policy");
        for( Iterator it = fields.iterator(); it.hasNext(); ){
            HierarchicalConfiguration sub = (HierarchicalConfiguration)it.next();
            String entityName = sub.getString(CONFIG_KEY_NAME);
            if(entityName.equals("stub_mode_encryption")){
                log.error("ENCRYPT? " + enableStubModeEncryption);
                enableStubModeEncryption = sub.getBoolean(CONFIG_KEY_VALUE);
                log.error("ENCRYPT? " + enableStubModeEncryption);
            }
        }
        return !dnsPorts.isEmpty();
    }
    
    public static boolean addressBelongsToLocalHost(String addrString) {
    	InetAddress addr = null;
    	try {
			addr = InetAddress.getByName(addrString);
		} catch (UnknownHostException e1) {
			log.error(e1.getMessage());
			return false;
		}
        //Loopback address?
        if (addr.isAnyLocalAddress() || addr.isLoopbackAddress()){
            return true;
        }
        //Assigned to one of the host's interfaces?
        try {
            return NetworkInterface.getByInetAddress(addr) != null;
        } catch (SocketException e) {
            return false;
        }
    }
    
    //public static void main(String[] args){}
}


