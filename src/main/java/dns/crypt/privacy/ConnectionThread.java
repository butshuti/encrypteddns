package dns.crypt.privacy;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Arrays;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.xbill.DNS.Header;
import org.xbill.DNS.Message;
import org.xbill.DNS.Name;
import org.xbill.DNS.Rcode;
import org.xbill.DNS.Record;
import org.xbill.DNS.Section;
import org.xbill.DNS.SimpleResolver;
import org.xbill.DNS.Type;

import dns.crypt.privacy.KeySpec.KeyType;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.xbill.DNS.Resolver;


public abstract class ConnectionThread implements Runnable {


	private static Message fakeMsg = null;
	protected byte[] keyBytes;
	protected String defaultName;
	protected AsymmetricKeyParameter serverPrivKey;
	protected Logger log = Logger.getLogger(this.getClass());
        protected Map<String, Boolean> upstreams;
        protected List<String> upstream_names;
        private Random rand;
        private static final int FLAGS_OFFSET = 4;
        
	public ConnectionThread(String defaultName, AsymmetricKeyParameter privKey, byte[] keyBytes, Map<String, Boolean> servers) {
		super();
		this.keyBytes = keyBytes;
		this.serverPrivKey = privKey;
		this.defaultName = defaultName;
                this.upstreams = servers;
                this.upstream_names = new ArrayList<String>(servers.keySet());
                if(this.upstreams == null || upstreams.isEmpty()){
                    log.error("No servers provided. Exiting with error.");
                    System.exit(1);
                }
                rand = new Random();
	}

	public abstract void run();
	
	protected Message handleRequest(Message request){
            Message msg = null;
            try {
                String address = upstream_names.get(rand.nextInt()%upstreams.size());
                Resolver r = new EncryptingResolver(address, upstreams.get(address));
                msg = r.send(request);
            } catch (Exception e){
                    msg = errorMsg(request.getHeader(), Rcode.SERVFAIL);
            }
            return msg;
        }
	
	protected byte[] handleRequest(byte[] buf, int dataLen, String clientAddr) throws IOException{
            boolean encrypting = CryptRecord.isEncrypting(buf);
            boolean keyRequested = CryptRecord.keyRequested(buf);
            KeySpec clientKey = null;
            Message request = null, errorMsg = null, response;
            byte[] headerBytes = new byte[Header.LENGTH];
            byte[] flagsBytes = new byte[CryptRecord.ENCRYPTABLE_SEGMENT_START];
	    System.arraycopy(buf, 0, headerBytes, 0, CryptRecord.ENCRYPTABLE_SEGMENT_START);
            headerBytes[FLAGS_OFFSET] |= 0x80;
            if(encrypting){
        	try {
        		byte[] temp = new byte[dataLen - CryptRecord.ENCRYPTABLE_SEGMENT_START];
        		clientKey = RecentClients.cachedKey(clientAddr);
        		System.arraycopy(buf, CryptRecord.ENCRYPTABLE_SEGMENT_START, temp, 0, temp.length);
        		if(clientKey != null){
        			temp = Enc.symDecrypt(temp, clientKey.getKey(), clientKey.getAlgorithm());
        		}else{
        			temp = Enc.asymDecrypt(temp, serverPrivKey);
        		}
				System.arraycopy(temp, 0, buf, CryptRecord.ENCRYPTABLE_SEGMENT_START, temp.length);
			} catch (Exception e) {
				errorMsg = errorMsg(new Header(headerBytes), Rcode.FORMERR);
				encrypting = false;
				keyRequested = true;
				log.error("Decryption error.");
				log.error(e.getMessage());
                                //throw new RuntimeException(e);
			}
        	if(errorMsg == null){
        		try{
        			request = new Message(CryptRecord.normalQuery(buf));
        		}catch(Exception e){
        			RecentClients.forgetHost(clientAddr);
                                errorMsg = errorMsg(new Header(headerBytes), Rcode.FORMERR);
                                encrypting = false;
                                keyRequested = true;
        		}
        		if(clientKey == null){
        			clientKey = CryptRecord.extractKey(request, KeyType.SYMMETRIC);
        			RecentClients.cacheKey(clientAddr, clientKey);
                                
        		}
        		if(clientKey == null){
                            errorMsg = errorMsg(new Header(headerBytes), Rcode.FORMERR);
                            encrypting = false;
                            keyRequested = true;
                            log.error("Unable to extract client key.");
                        }
        	}
        }else{
        	request = new Message(CryptRecord.normalQuery(buf));
        }
        if(errorMsg == null){
        	request.removeAllRecords(Section.ADDITIONAL);  
        	response = handleRequest(request);
        }else{
        	response = errorMsg;
        }
        if(keyRequested || request.getQuestion().getType() == Type.KEY){       
        	Record cryptRecord = CryptRecord.keyEnvelope(keyBytes, Enc.EncMode.RSA.getID(), 2592000);
                byte[] hdrBytes = response.getHeader().toWire();
        	response.setHeader(new Header(CryptRecord.keyUpdateQuery(hdrBytes)));
        	response.addRecord(cryptRecord, Section.ADDITIONAL);
        }
        byte[] retBuf = response.toWire();
        if(encrypting){
        	try{
        		byte[] temp = new byte[retBuf.length - CryptRecord.ENCRYPTABLE_SEGMENT_START];
        		System.arraycopy(retBuf, CryptRecord.ENCRYPTABLE_SEGMENT_START, temp, 0, temp.length);
                        temp = Enc.symEncrypt(temp, clientKey.getKey(), clientKey.getAlgorithm());
                        byte[] newRetBuf = new byte[temp.length + CryptRecord.ENCRYPTABLE_SEGMENT_START];
                        System.arraycopy(retBuf, 0, newRetBuf, 0, CryptRecord.ENCRYPTABLE_SEGMENT_START);
                        System.arraycopy(temp, 0, newRetBuf, CryptRecord.ENCRYPTABLE_SEGMENT_START, temp.length);
                        return newRetBuf;
            }catch(Exception e){
            	log.error(e.getMessage());
            }
        }
        return retBuf;
	}
	
	protected Message errorMsg(Header hdr, int errCode){
            Message msg = new Message();
            if(errCode == Rcode.NOERROR){
                hdr.setRcode(Rcode.SERVFAIL);
            }else{
                hdr.setRcode(errCode);
            }
            msg.setHeader(hdr);
            return msg;
	}
}
