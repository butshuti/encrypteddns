package dns.crypt.privacy;

import java.util.HashMap;
import java.util.Map;

public class SecretCache {
	private static Map<String, KeyRecord> cache;
	private static long ttl = 3600;
	static boolean enableCache = false;
	
	static{
		if(cache == null){
			cache = new HashMap<String, KeyRecord>();
		}
	}
	
	public static void enableCache(boolean bit){
		enableCache = bit;
	}
	
	public static KeySpec cachedKey(String addr){
		if(!enableCache){
			return null;
		}
		KeyRecord kr = cache.get(addr);
		if(kr == null || (kr.ts + ttl) > System.currentTimeMillis()){
			return null;
		}
		return kr.key;
	}
	
	public static void cacheKey(String addr, KeySpec key){
		if(enableCache){
			cache.put(addr, new KeyRecord(key, ttl));
		}
	}
	
	public static void forgetHost(String addr){
		cache.remove(addr);
	}
	
	private static class KeyRecord{
		private KeySpec key;
		private long ts;
		KeyRecord(KeySpec key, long ts){
			this.key = key;
			this.ts = ts;
		}
	}
}
