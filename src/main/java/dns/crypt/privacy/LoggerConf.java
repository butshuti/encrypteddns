package dns.crypt.privacy;

import org.apache.log4j.xml.DOMConfigurator;

public class LoggerConf {

	public static void init(String loggerConfPath){
		DOMConfigurator.configure(loggerConfPath);
	}
}
