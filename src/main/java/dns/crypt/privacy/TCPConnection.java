package dns.crypt.privacy;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

import java.util.List;

import java.util.Map;

import org.bouncycastle.crypto.params.AsymmetricKeyParameter;

public class TCPConnection extends ConnectionThread{
	private Socket socket;
	
	public TCPConnection(String defaultName, Socket socket, AsymmetricKeyParameter privKey, byte[] keyBytes, Map<String, Boolean> servers) {
		super(defaultName, privKey, keyBytes, servers);
		this.socket = socket;
	}

	@Override
	public void run() {

		try{

			try {
				int inLength;
				DataInputStream dataIn;
				DataOutputStream dataOut;
				byte[] in;

				InputStream is = socket.getInputStream();
				dataIn = new DataInputStream(is);
				inLength = dataIn.readUnsignedShort();
				in = new byte[inLength];
				dataIn.readFully(in);
				byte[] response = handleRequest(in, inLength, socket.getRemoteSocketAddress().toString());
				dataOut = new DataOutputStream(socket.getOutputStream());
				dataOut.writeShort(response.length);
				dataOut.write(response);
			} catch (IOException e) {

				log.debug("Error sending TCP response to " + socket.getRemoteSocketAddress() + ":" + socket.getPort() + ", " + e);

			} finally {
				try {
					socket.close();
				} catch (IOException e) {
				}
			}

		}catch(Throwable e){

			log.error("Error processing TCP connection from " + socket.getRemoteSocketAddress() + ":" + socket.getPort());
		}
	}

}
