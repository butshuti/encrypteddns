package dns.crypt.privacy;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.io.*;
import java.net.*;

import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;

import org.apache.log4j.Logger;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.xbill.DNS.Flags;
import org.xbill.DNS.Header;
import org.xbill.DNS.Message;
import org.xbill.DNS.Name;
import org.xbill.DNS.OPTRecord;
import org.xbill.DNS.Opcode;
import org.xbill.DNS.Options;
import org.xbill.DNS.Rcode;
import org.xbill.DNS.Record;
import org.xbill.DNS.Resolver;
import org.xbill.DNS.ResolverConfig;
import org.xbill.DNS.ResolverListener;
import org.xbill.DNS.Section;
import org.xbill.DNS.TSIG;
import org.xbill.DNS.Type;
import org.xbill.DNS.WireParseException;
import org.xbill.DNS.ZoneTransferException;
import org.xbill.DNS.ZoneTransferIn;

import dns.crypt.privacy.KeySpec.KeyType;
import dns.crypt.reflectxbill.TCPClient;
import dns.crypt.reflectxbill.UDPClient;

import java.security.spec.InvalidKeySpecException;

/**
 * An implementation of Resolver that supports encrypted queries.
 * @see SimpleResolver
 * @see TSIG
 * @see OPTRecord
 *
 */


public class EncryptingResolver implements Resolver {

/** The default port to send queries to */
public static final int DEFAULT_PORT = 53;

/** The default EDNS payload size */
public static final int DEFAULT_EDNS_PAYLOADSIZE = 1280;

/** The header length */

private InetSocketAddress address;
private InetSocketAddress localAddress;
private boolean useTCP, ignoreTruncation;
private OPTRecord queryOPT;
private TSIG tsig;
private long timeoutValue = 10 * 1000;

private static final short DEFAULT_UDPSIZE = 512;

private static String defaultResolver = "localhost", curResolver="";
private static int uniqueID = 0;

KeyGenerator keygenerator;
public static final int RECORDS_START = 0;//CryptRecord.ENCRYPTABLE_SEGMENT_START;
private static KeySpec key;
private static AsymmetricKeyParameter serverKey;
private  static boolean encryptMode, trustedServer, keyCached=false;
private Logger log = Logger.getLogger(this.getClass());

private boolean cacheSecrets = false;

public EncryptingResolver(String hostname, boolean encrypt) throws UnknownHostException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
                                                                       InvalidKeySpecException, ClassNotFoundException{
	if (hostname == null) {
		hostname = ResolverConfig.getCurrentConfig().server();
		if (hostname == null)
			hostname = defaultResolver;
	}
	InetAddress addr;
	if (hostname.equals("0"))
		addr = InetAddress.getLocalHost();
	else
		addr = InetAddress.getByName(hostname);
	address = new InetSocketAddress(addr, DEFAULT_PORT);
	curResolver = "CLIENT_" + address.getAddress().getHostAddress();
	serverKey = PKIUtils.fetchHostKey(hostname);
	trustedServer = (serverKey != null);
	encryptMode = encrypt;
	keygenerator = KeyGenerator.getInstance(Enc.EncMode.DEFAULT.getEncryptionAlg());
}

public void enableCachedSecrets(boolean flag){
	cacheSecrets = flag;
	TrustedServers.enableCache(flag);
}

public EncryptingResolver() throws UnknownHostException, NoSuchAlgorithmException, InvalidKeyException, InvalidKeySpecException, NoSuchPaddingException, 
                                    ClassNotFoundException {
	this(null, false);
}

InetSocketAddress getAddress() {
	return address;
}

/** Sets the default host (initially localhost) to query */
public static void setDefaultResolver(String hostname) throws NoSuchAlgorithmException, InvalidKeySpecException, ClassNotFoundException {
	defaultResolver = hostname;
	try {
		serverKey = PKIUtils.fetchHostKey(hostname);
	} catch (UnknownHostException e) {
		e.printStackTrace();
		trustedServer = false;
		return;
	}
	trustedServer = (serverKey != null);
}

public void setPort(int port) {
	address = new InetSocketAddress(address.getAddress(), port);
}

/**
 * Sets the address of the server to communicate with.
 * @param addr The address of the DNS server
 */
public void setAddress(InetSocketAddress addr) {
	address = addr;
}

/**
 * Sets the address of the server to communicate with (on the default
 * DNS port)
 * @param addr The address of the DNS server
 */
public void setAddress(InetAddress addr) {
	address = new InetSocketAddress(addr, address.getPort());
}

/**
 * Sets the local address to bind to when sending messages.
 * @param addr The local address to send messages from.
 */
public void setLocalAddress(InetSocketAddress addr) {
	localAddress = addr;
}

/**
 * Sets the local address to bind to when sending messages.  A random port
 * will be used.
 * @param addr The local address to send messages from.
 */
public void setLocalAddress(InetAddress addr) {
	localAddress = new InetSocketAddress(addr, 0);
}

public void setTCP(boolean flag) {
	this.useTCP = flag;
}

public void setIgnoreTruncation(boolean flag) {
	this.ignoreTruncation = flag;
}

public void setEDNS(int level, int payloadSize, int flags, @SuppressWarnings("rawtypes") List options) {
	if (level != 0 && level != -1)
		throw new IllegalArgumentException("invalid EDNS level - " +
						   "must be 0 or -1");
	if (payloadSize == 0)
		payloadSize = DEFAULT_EDNS_PAYLOADSIZE;
	queryOPT = new OPTRecord(payloadSize, 0, level, flags, options);
}

public void setEDNS(int level) {
	setEDNS(level, 0, 0, null);
}

public void setTSIGKey(TSIG key) {
	tsig = key;
}

TSIG getTSIGKey() {
	return tsig;
}

public void setTimeout(int secs, int msecs) {
	timeoutValue = (long)secs * 1000 + msecs;
}

public void setTimeout(int secs) {
	setTimeout(secs, 0);
}

long getTimeout() {
	return timeoutValue;
}

private Message parseMessage(byte [] b) throws WireParseException {
	try {
		return (new Message(b));
	}
	catch (IOException e) {
		if (Options.check("verbose"))
			e.printStackTrace();
		if (!(e instanceof WireParseException))
			e = new WireParseException("Error parsing message");
		throw (WireParseException) e;
	}
}

private void verifyTSIG(Message query, Message response, byte [] b, TSIG tsig) {
	if (tsig == null)
		return;
	int error = tsig.verify(response, b, query.getTSIG());
	if (Options.check("verbose"))
		log.error("TSIG verify: " + Rcode.string(error));
}

private void applyEDNS(Message query) {
	if (queryOPT == null || query.getOPT() != null)
		return;
	query.addRecord(queryOPT, Section.ADDITIONAL);
}

private int maxUDPSize(Message query) {
	OPTRecord opt = query.getOPT();
	if (opt == null)
		return DEFAULT_UDPSIZE;
	else
		return opt.getPayloadSize();
}

/**
 * Sends a message to a single server and waits for a response.  No checking
 * is done to ensure that the response is associated with the query.
 * @param query The query to send.
 * @return The response.
 * @throws IOException An error occurred while sending or receiving.
 */
public Message send(Message query) throws IOException{
	if (Options.check("verbose"))
		log.error("Sending to " +
				   address.getAddress().getHostAddress() +
				   ":" + address.getPort());

	if (query.getHeader().getOpcode() == Opcode.QUERY) {
		Record question = query.getQuestion();
		if (question != null && question.getType() == Type.AXFR)
			return sendAXFR(query);
	}

	query = (Message) query.clone();
	applyEDNS(query);
	if (tsig != null)
		tsig.apply(query, null);
	key = TrustedServers.cachedKey(curResolver);
	if(key==null || !cacheSecrets){
		key = new KeySpec(keygenerator.generateKey().getEncoded(), Enc.EncMode.DEFAULT.getID(), 0, "CLIENT", true);
		if(cacheSecrets){
			TrustedServers.cacheKey(curResolver, key);
		}
	}else{
		keyCached = true;
	}
	byte [] out = encrypt(query, serverKey, key);
	int udpSize = maxUDPSize(query);
	boolean tcp = false;
	long endTime = System.currentTimeMillis() + timeoutValue;
	do {
		byte[] in, processedIn;

		if (useTCP || out.length > udpSize)
			tcp = true;
                if (tcp){
			in = TCPClient.sendrecv(localAddress, address, out,	endTime);
                }
                else{
			in = UDPClient.sendrecv(localAddress, address, out, udpSize, endTime);
                }
                processedIn = decrypt(in, key);
		/*
		 * Check that the response is long enough.
		 */
		if (in.length < CryptRecord.ENCRYPTABLE_SEGMENT_START) {
			throw new WireParseException("invalid DNS header - " +
						     "too short");
		}
		/*
		 * Check that the response ID matches the query ID.  We want
		 * to check this before actually parsing the message, so that
		 * if there's a malformed response that's not ours, it
		 * doesn't confuse us.
		 */
		int id = ((in[0] & 0xFF) << 8) + (in[1] & 0xFF);
		int qid = query.getHeader().getID();
		if (id != qid) {
			String error = "invalid message id: expected " + qid +
				       "; got id " + id;
			if (tcp) {
				throw new WireParseException(error);
			} else {
				if (Options.check("verbose")) {
					log.error(error);
				}
				continue;
			}
		}
		Message response = null;
		try {
			response = parseMessage(processedIn);
		} catch (WireParseException e) {
			response = null;
		}
		if(encryptMode && CryptRecord.keyRequested(in)){
			KeySpec keySpec;
			if(response != null){
				keySpec = CryptRecord.extractKey(response, KeyType.ASYMMETRIC);
			}else{
				keySpec = CryptRecord.extractKey(new Message(in), KeyType.ASYMMETRIC);
			}
                        System.out.println("KEYSPEC: " + keySpec + "/ " + Arrays.toString(keySpec.getKeyBytes()));
			String serverKeyFile = PKIUtils.saveRawKeyInfo(keySpec.getKeyBytes(), keySpec.getOwner(), keySpec.getTTL());
                        try {
                            serverKey = PKIUtils.loadPublicKeyKeyFromFile(serverKeyFile);
                        } catch (Exception e) {
                            throw new IOException(e);
                        }
                trustedServer = true;
		}
		response = response != null ? response : parseMessage(in);
		verifyTSIG(query, response, in, tsig);
		if (!tcp && !ignoreTruncation &&
		    response.getHeader().getFlag(Flags.TC))
		{
			tcp = true;
			continue;
		}
		return response;
	} while (true);
}

/**
 * Asynchronously sends a message to a single server, registering a listener
 * to receive a callback on success or exception.  Multiple asynchronous
 * lookups can be performed in parallel.  Since the callback may be invoked
 * before the function returns, external synchronization is necessary.
 * @param query The query to send
 * @param listener The object containing the callbacks.
 * @return An identifier, which is also a parameter in the callback
 */
public Object sendAsync(final Message query, final ResolverListener listener) {
	final Object id;
	synchronized (this) {
		id = new Integer(uniqueID++);
	}
	Record question = query.getQuestion();
	String qname;
	if (question != null)
		qname = question.getName().toString();
	else
		qname = "(none)";
	String name = this.getClass() + ": " + qname;
	Thread thread = new ResolveThread(this, query, id, listener);
	thread.setName(name);
	thread.setDaemon(true);
	thread.start();
	return id;
}

private byte[] encrypt(Message msg, AsymmetricKeyParameter publicKey, KeySpec sharedSecret){
	byte[] msgBytes;
        if(encryptMode && trustedServer){
		try {
                    byte[] sharedKey = sharedSecret.getKey().getEncoded();
                    Record crypt = CryptRecord.keyEnvelope(sharedKey, sharedSecret.getAlgorithm(), 0);
                    msg.addRecord(crypt, Section.ADDITIONAL);
                    msgBytes = msg.toWire();
                    msgBytes[CryptRecord.HEADER_ENCRYPT_BYTE_OFFS] |= CryptRecord.ENCRYPT_BIT_FLAG;
                    byte[] temp = new byte[msgBytes.length - CryptRecord.ENCRYPTABLE_SEGMENT_START];
                    System.arraycopy(msgBytes, CryptRecord.ENCRYPTABLE_SEGMENT_START, temp, 0, temp.length);
                    //temp = cachedSecret ? Enc.symEncrypt(temp, key.getKey(), key.getAlgorithm()) : Enc.asymEncrypt(temp, publicKey);
		    temp = Enc.asymEncrypt(temp, publicKey);
                    byte[] encryptedBytes = new byte[temp.length + CryptRecord.ENCRYPTABLE_SEGMENT_START];
                    System.arraycopy(msgBytes, 0, encryptedBytes, 0, CryptRecord.ENCRYPTABLE_SEGMENT_START);
                    System.arraycopy(temp, 0, encryptedBytes, CryptRecord.ENCRYPTABLE_SEGMENT_START, temp.length);
                    return encryptedBytes;
		}catch(Exception e){
                    log.error(e.getMessage());
                    //return null;
                    throw new RuntimeException(e);
		}
	}else if(encryptMode){
		msgBytes = CryptRecord.keyUpdateQuery(msg.toWire());
	}else{
		msgBytes = msg.toWire();
	}
	return msgBytes;
}

private byte[] decrypt(byte[] msgBytes, KeySpec secret){
	if(encryptMode && trustedServer){
		try {
			byte[] temp = new byte[msgBytes.length - CryptRecord.ENCRYPTABLE_SEGMENT_START];
                        System.arraycopy(msgBytes, CryptRecord.ENCRYPTABLE_SEGMENT_START, temp, 0, temp.length);
			temp = Enc.symDecrypt(temp, secret.getKey(), secret.getAlgorithm());
			byte[] decryptedBytes = new byte[temp.length + CryptRecord.ENCRYPTABLE_SEGMENT_START];
			System.arraycopy(msgBytes, 0, decryptedBytes, 0, CryptRecord.ENCRYPTABLE_SEGMENT_START);
			System.arraycopy(temp, 0, decryptedBytes, CryptRecord.ENCRYPTABLE_SEGMENT_START, temp.length);
			return decryptedBytes;
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
			return null;
		} 
	}
	return msgBytes;
}

@SuppressWarnings("unchecked")
private Message sendAXFR(Message query) throws IOException {
	Name qname = query.getQuestion().getName();
	ZoneTransferIn xfrin = ZoneTransferIn.newAXFR(qname, address, tsig);
	xfrin.setTimeout((int)(getTimeout() / 1000));
	xfrin.setLocalAddress(localAddress);
	try {
		xfrin.run();
	}
	catch (ZoneTransferException e) {
		throw new WireParseException(e.getMessage());
	}
	List<Record> records = xfrin.getAXFR();
	Message response = new Message(query.getHeader().getID());
	response.getHeader().setFlag(Flags.AA);
	response.getHeader().setFlag(Flags.QR);
	response.addRecord(query.getQuestion(), Section.QUESTION);
	Iterator<Record> it = records.iterator();
	while (it.hasNext())
		response.addRecord(it.next(), Section.ANSWER);
	return response;
}

}