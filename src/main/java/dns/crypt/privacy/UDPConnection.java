package dns.crypt.privacy;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

import java.util.List;

import java.util.Map;

import org.bouncycastle.crypto.params.AsymmetricKeyParameter;


public class UDPConnection extends ConnectionThread {


	private final DatagramPacket inDataPacket;
	private DatagramSocket socket;
	
	public UDPConnection(String defaultName, DatagramSocket socket, DatagramPacket inDataPacket, AsymmetricKeyParameter privKey, byte[] keyBytes, Map<String, Boolean> servers) {
		super(defaultName, privKey, keyBytes, servers);
		this.socket = socket;
		this.inDataPacket = inDataPacket;
	}

	@Override
	public void run() {

		try {
            byte[] buf = inDataPacket.getData();
            log.error("Packet!");                               
            byte[] retBuf = handleRequest(buf, inDataPacket.getLength(), inDataPacket.getAddress().getHostAddress());
            DatagramPacket packet = new DatagramPacket(retBuf, retBuf.length, inDataPacket.getAddress(), inDataPacket.getPort());
            packet.setData(retBuf);
            packet.setLength(retBuf.length);
            packet.setAddress(inDataPacket.getAddress());
            packet.setPort(inDataPacket.getPort());
            try {
                    socket.send(packet);
            } catch (IOException e) {
                    log.error("Error sending UDP response to " + inDataPacket.getAddress() + ", " + e);
            }
        } catch (IOException e) {
        	log.error(e.getMessage());
        }
	}
}
