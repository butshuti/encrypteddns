package dns.crypt.privacy;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;

import org.apache.log4j.Logger;
import org.bouncycastle.crypto.AsymmetricBlockCipher;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.encodings.PKCS1Encoding;
import org.bouncycastle.crypto.engines.RSAEngine;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;

public class BCEnc extends Enc{
		
	protected static byte[] encrypt(byte[] msgBytes, Key key, int alg, boolean useIV) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IOException, InvalidAlgorithmParameterException{
		byte[] cryptBytes;
		int cryptLen = msgBytes.length - RECORDS_START;
		cryptBytes = new byte[cryptLen];
		System.arraycopy(msgBytes, RECORDS_START, cryptBytes, 0, cryptLen);
		try {
			cipher = Cipher.getInstance(EncMode.getForm(alg, DEFAULT_PADD_MODE));
			if(useIV){
				cipher.init(Cipher.ENCRYPT_MODE, key, ivSpec);
			}else{
				cipher.init(Cipher.ENCRYPT_MODE, key);
			}
			cryptBytes = cipher.doFinal(cryptBytes);
		} catch (IllegalBlockSizeException e1) {
			log.error(e1.getMessage());
			return null;
		} catch (BadPaddingException e1) {
			log.error(e1.getMessage());
			return null;
		}
		System.arraycopy(cryptBytes, 0, msgBytes, RECORDS_START, cryptLen);
		return msgBytes;
	}
	
	protected static byte[] decrypt(byte[] msgBytes, Key key, int alg, boolean useIV) 
			throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IOException, InvalidAlgorithmParameterException{
		byte[] cryptBytes = new byte[msgBytes.length];
		int cryptLen = msgBytes.length - RECORDS_START;
		cryptBytes = new byte[cryptLen];
		System.arraycopy(msgBytes, RECORDS_START, cryptBytes, 0, cryptLen);
		try {
			cipher = Cipher.getInstance(EncMode.getForm(alg, DEFAULT_PADD_MODE));
			if(useIV){
				cipher.init(Cipher.DECRYPT_MODE, key, ivSpec);
			}else{
				cipher.init(Cipher.DECRYPT_MODE, key);
			}
			cryptBytes = cipher.doFinal(msgBytes);
		}catch (IllegalBlockSizeException e1) {
			log.error(e1.getMessage());
			return null;
		} catch (BadPaddingException e1) {
			log.error(e1.getMessage());
			return null;
		}
		System.arraycopy(cryptBytes, 0, msgBytes, RECORDS_START, cryptLen);
		return msgBytes;
	}
	
	protected static byte[] AsymBCProcess(byte[] inputBytes, AsymmetricKeyParameter key, boolean encrypt) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IOException, InvalidCipherTextException{
		AsymmetricBlockCipher bc = new PKCS1Encoding(new RSAEngine());
		bc.init(encrypt, key);
		int inputOffs=0, outputOffs=0, blockSize=bc.getInputBlockSize(), outBS=bc.getOutputBlockSize(), inputLen=inputBytes.length;
		int outputLen;
		if(encrypt){
			outputLen = (outBS/blockSize)*inputLen;
			outputLen += (outBS - (outputLen % outBS));
		}else{
			outputLen = (blockSize/outBS)*inputLen;
			outputLen += (outBS - (outputLen % outBS));
		}
		byte[] outputBytes = new byte[outputLen];
		while(inputOffs < inputLen){
			blockSize = Math.min(blockSize, inputLen - inputOffs);
			byte[] temp = bc.processBlock(inputBytes, inputOffs, blockSize);
			System.arraycopy(temp, 0, outputBytes, outputOffs, temp.length);
			inputOffs += blockSize;
			outputOffs += temp.length;
			if(temp.length < bc.getOutputBlockSize())break;
		}
		byte[] ret = new byte[outputOffs];
		System.arraycopy(outputBytes, 0, ret, 0, outputOffs);
		return ret;
	}
}
