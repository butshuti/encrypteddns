package dns.crypt.privacy;

import java.net.InetAddress;
import java.net.UnknownHostException;
import org.apache.log4j.Logger;
import org.xbill.DNS.DClass;
import org.xbill.DNS.Message;
import org.xbill.DNS.Name;
import org.xbill.DNS.Record;
import org.xbill.DNS.Section;
import org.xbill.DNS.TextParseException;
import dns.crypt.privacy.KeySpec.KeyType;
import java.io.IOException;
import java.util.Arrays;
import org.xbill.DNS.DNSKEYRecord;
import org.xbill.DNS.DNSSEC;
import org.xbill.DNS.Header;

public class CryptRecord {

	public static final int HEADER_KEY_REQ_BYTE_OFFS = 3;
	public static final int HEADER_ENCRYPT_BYTE_OFFS = 2;
        public static final int RR_KEY_OFFSET = 3;
        public static final int DNSKEY_RR_PARAMS_OFFSET = 4;
	public static final int ENCRYPT_BIT_FLAG = 0x40;
	public static final int KEY_REQ_BIT_FLAG = 0x40;
        public static final int ENCRYPTABLE_SEGMENT_START = HEADER_ENCRYPT_BYTE_OFFS + 2;
	private static Logger log = Logger.getLogger(CryptRecord.class);


	public static boolean isEncrypting(byte[] buf) {
		return (buf[HEADER_ENCRYPT_BYTE_OFFS]&ENCRYPT_BIT_FLAG) != 0;
	}

	public static boolean keyRequested(byte[] buf) {
		return (buf[HEADER_KEY_REQ_BYTE_OFFS]&KEY_REQ_BIT_FLAG) != 0;
	}
	
	public static KeySpec extractKey(Message request, KeySpec.KeyType keyType){
		Record[] additional = request.getSectionArray(Section.ADDITIONAL);
		if(additional != null && additional.length > 0){
			Record cryptRecord = additional[additional.length - 1];
			byte[] recBytes = cryptRecord.rdataToWireCanonical();
			int keyLen = 0x0FF & recBytes[DNSKEY_RR_PARAMS_OFFSET];
			keyLen |= ((int)recBytes[DNSKEY_RR_PARAMS_OFFSET+1] << Byte.SIZE);
			int algorithmID = recBytes[DNSKEY_RR_PARAMS_OFFSET+2];
			if(recBytes.length >= keyLen + DNSKEY_RR_PARAMS_OFFSET + RR_KEY_OFFSET){
				byte[] keyBytes = new byte[keyLen];
                                System.arraycopy(recBytes, DNSKEY_RR_PARAMS_OFFSET + RR_KEY_OFFSET, keyBytes, 0, keyLen);
                                System.out.println(String.format("KEY (%d): %s", keyLen, Arrays.toString(keyBytes)));
				try {
//					log.debug("=====================");
//					log.debug("secretKey record:" + Arrays.toString(recBytes));
//					log.debug("ALGORITHM: " + algorithmID);
//					log.debug("=====================");
                                    
                                    return new KeySpec(keyBytes, algorithmID, cryptRecord.getTTL(), cryptRecord.getName().toString(), keyType.equals(KeyType.SYMMETRIC));
				} catch (Exception e) {
					log.error(e.getMessage());
					throw new RuntimeException(e);
				}
			}
		}
		return null;
	}
	
	public static Record keyEnvelope(byte[] keyBytes, int algorithmID, long ttl){
                byte[] creptRecordBytes = new byte[keyBytes.length + RR_KEY_OFFSET];
		creptRecordBytes[0] = (byte) keyBytes.length;
		creptRecordBytes[1] = (byte) (keyBytes.length >> Byte.SIZE);
		creptRecordBytes[2] = (byte) algorithmID;
	                    
		System.arraycopy(keyBytes, 0, creptRecordBytes, RR_KEY_OFFSET, keyBytes.length);
		Name name;
		try {
			name = Name.fromString(InetAddress.getLocalHost().getHostName() + ".");
		} catch (TextParseException e) {
			name = Name.root;
		} catch (UnknownHostException e) {
			name = Name.root;
		}
		Record crypt = new DNSKEYRecord(name, DClass.IN, ttl, 0, 0, DNSSEC.Algorithm.RSASHA1, creptRecordBytes);
		return crypt;
	}

	public static byte[] keyUpdateQuery(byte[] buf) {
		buf[CryptRecord.HEADER_KEY_REQ_BYTE_OFFS] |= CryptRecord.KEY_REQ_BIT_FLAG;
		return buf;
	}
	public static byte[] normalQuery(byte[] buf) {
		buf[CryptRecord.HEADER_KEY_REQ_BYTE_OFFS] &= ~CryptRecord.KEY_REQ_BIT_FLAG;
		buf[CryptRecord.HEADER_ENCRYPT_BYTE_OFFS] &= ~CryptRecord.ENCRYPT_BIT_FLAG;
		return buf;
	}
}
